package me.leon

const val VERSION = "1.7.0"
const val REPO_URL = "https://github.com/Leon406/ToolsFx"
const val PJ52_URL = "https://www.52pojie.cn/thread-1501153-1-1.html"
const val LAN_ZOU_DOWNLOAD_URL = "https://leon.lanzoui.com/b0d9av2kb"
const val CHECK_UPDATE_URL = "https://ghproxy.com/https://raw.githubusercontent.com/Leon406/ToolsFx/main/current"
const val CHECK_UPDATE_URL2 = "https://gitee.com/LeonShih/ToolsFx/raw/main/current"
const val LICENSE = "https://cdn.staticaly.com/gh/Leon406/ToolsFx/main/LICENSE"
