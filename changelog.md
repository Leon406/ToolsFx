# v1.5.0  
## feature:  
- feat:二维码加入快捷键
- feat:美化非对称加密,签名,二维码样式
- feat:美化对称加密，哈希，MAC样式
- feat:编码及转换样式优化
- feat:编码及编码转换样式美化
- feat:增加二维码提示
- feat:签名与验签模块
- feat:二维码功能完善
- feat: qrcode 识别与生成,截屏识别demo
- feat: 对称加密支持编码集选择及拖动文件优化
## bug fix:  
- fix: toast 异常
- fix:编码转换 hex转base64错误
- fix:编码转换异常显示
# v1.5.1  
## feature:  
- feat:新增base58/base58check功能
- feat:二维码识别 新增剪贴板图片
## bug fix:  
- fix:编码转换urlencode错误及hash大文件按钮点击
- fix:快捷键冲突
# v1.6.0  
## feature:  
- feat: 检测升级功能
- feat: 新增关于模块
- feat: 编码/转换新增 base32/base62/base85/base91 功能
- feat: 更换复制文字为图片
- feat:对称加密文件加密 文件名称优化
- feat:rsa支持pk8 和pem文件加解密
- feat:调整复制按钮位置，新增剪贴板导入
- feat:加入app名字和版本配置
## bug fix:  
- fix: 时间小时格式
- fix: 底部提示默认不显示
- fix:base58/base58check部分字符解码错误 
# v1.7.0  
## feature:  
- feat: 新增字符处理模块
- feat: add new encode octal/decimal/escape
- feat: 增强hex/unicode解码
- feat: 右键置顶/语言选择功能
- feat: 编码base系列支持自定义字典
- feat: 新增base92
- feat: i18n 
- feat: 新增jre/vm信息显示, 置顶APP
- feat: UI美化
- feat: hash/对称加密支持多个文件操作

